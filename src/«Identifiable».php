<?php

declare(strict_types=1);

/**
 * An object identifiable by a unique integer.
 */
interface «Identifiable»
 {
    /**
     * returns the objects unique integer identifier.
     */
    public function getId(): int;
 }
